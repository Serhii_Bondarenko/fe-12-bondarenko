class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    get name() {
        return this._name;
    }

    set name (nameValue) {
        if (nameValue.length < 2) {
           alert("Name length > 2");
            return;
        }
        this._name = nameValue;
    }

    get age (){
        return this._age
    }

    set age(ageValue){
        if(ageValue <= 0){
            alert("Sorry! Age > 0");
            return;
        }
        this._age = ageValue
    }


    get salary(){
        return this._salary
    }

    set salary(salaryValue){
        if (salaryValue < 0){
            alert("Sorry! Salary => 0");
            return;
        }
        this._salary = salaryValue
    }


}

const sergio = new Employee('Sergio', 18, 3000000)
console.log(sergio);


class Programmer extends Employee{
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    get lang(){
        return this._lang
    }

    set lang(valueLang){
        this._lang = valueLang;
    }

    set salary(value) {
        super.salary = value;
    }


    get salary(){
        return 3 * this._salary;
    }

}

const alex = new Programmer("alex", 18, 40, 'en');
console.log(alex);

const rita = new Programmer('rita', 22, 10000, 'ua/en');
console.log(rita);