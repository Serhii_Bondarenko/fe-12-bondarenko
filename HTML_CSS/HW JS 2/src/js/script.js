const bookList = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const keys = ['author', 'name', 'price'];

const createDOMElement = (tag, parentElement, ...attributes) => {
    let element = document.createElement(tag);
    parentElement.append(element);

    if(attributes.length) {
        Object.entries(attributes[0]).forEach(([key, value]) => element[key] = value);
    }
    return element;
}


const renderBook = (book, booksWrapper) => {
    const li = createDOMElement('li', booksWrapper, {className: 'item-list'});

    li.innerHTML = Object.entries(book).map(([_,value]) => value).join(', ');
}

const validation = (books, validationKeys, booksWrapper) => {
    books.forEach(book => {
        try{
           const missingKeys = validationKeys.filter(item => !Object.keys(book).includes(item))
             if(missingKeys.length > 0){
                 throw new Error(`Missing keys: ${missingKeys.join(', ')} `);
             }else {
                 renderBook(book, booksWrapper);
             }
        } catch(err) {
            console.error(err)
        }
    })
}


const showBookList = () => {
    const body = document.querySelector('body');
    const div = createDOMElement('div', body, {id: 'root'});
    const listWrapper = createDOMElement('ul', div);

    validation(bookList, keys, listWrapper);
}

showBookList();




