const FILMS_URL = 'https://ajax.test-danit.com/api/swapi/films'

const createDOMElement = (tag, parentElement) => {
    let element = document.createElement(tag);
    parentElement.append(element);
    return element;
}

const showFilmDetails = (parentTag, film) => {
    const li = createDOMElement('li', parentTag);
    li.innerHTML = `Episode ${film.episodeId} - '${film.name}'\<br> ${film.openingCrawl} :`;
    return li;
}

const getDataByUrl = (url) => {
    const headers = {'Content-Type': 'application/json'};
    return fetch(url,{
        method:'GET',
        headers
    })
}

const getFilms = (url, parentTag) => {
    const getFilmData = getDataByUrl(url);
    getFilmData
    .then(response => {
        if(response.ok){
            return response.json()
        }
        throw new Error (`Error ${response.statusText}`);
    }).then(data => {
        data.forEach(film => {
           const filmLi = showFilmDetails(parentTag, film);
           const ul= createDOMElement('ul',filmLi);
            const characterPromises = film.characters.map(characterUrl => getDataByUrl(characterUrl))
            const allCharactersPromises = Promise.all([...characterPromises])
            allCharactersPromises.then(characters =>
                characters.forEach(character => character.json().then(character => {
                    const characterLi = createDOMElement('li',ul);
                    characterLi.innerText = character.name
                }))
            ).catch(err => console.error(err))
        })
    }).catch(err => console.error(err))
}

const showFilmsWithCharacters = () => {
    const body = document.querySelector('body');
    const ul = createDOMElement('ul', body);
    getFilms(FILMS_URL, ul);
}

showFilmsWithCharacters()














