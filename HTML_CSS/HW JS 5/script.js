const usersApi = 'https://ajax.test-danit.com/api/json/users';
const postsApi = 'https://ajax.test-danit.com/api/json/posts';



class Twitter{
    constructor(postsApi, usersApi) {
        this.postsApi = postsApi;
        this.usersApi = usersApi;
    }

     getPosts() {
        const headers = {'Content-Type': 'application/json'};
        fetch(this.postsApi,{
            method:'GET',
            headers:headers
        }).then(response => {
            if(response.ok){
                return response.json()
            }
            throw  new Error (`Error ${response.statusText}`);

        }).then(posts => {
            fetch(this.usersApi,{
                method:'GET',
                headers:headers
            }).then(response => {
                if (response.ok) {
                    return response.json()
                }
                throw  new Error(`Error ${response.statusText}`);
            }).then(users => {
                const body = document.querySelector('body');
                const postsWrapper = this.createDOMElement('div',body )
                postsWrapper.className = 'posts-wrapper';

                posts.forEach(post => {
                    const currentUser = users.find(user => user.id === post.userId);
                    const card = new Card(post.id, post.title, post.body, currentUser.name, currentUser.email).render()
                    postsWrapper.append(card)
                })
            })
        })
            .catch(err => {console.log(err)})
    }

     createDOMElement(tag, parentElement){
        let element = document.createElement(tag);
        parentElement.append(element);
        return element;
    }

}


class Card {
    constructor(id, title, body, authorName, authorEmail) {
        this.id = id;
        this.title = title
        this.authorName = authorName;
        this.authorEmail = authorEmail;
        this.body = body;
    }

    render(){
        const div = document.createElement('div');
        div.addEventListener('click', (event) => this.deletePost(event))
        div.classList.add('post-wrapper')
        div.id = `${this.id}`
        div.innerHTML = `<p class='author-name'>${this.authorName}<span>${this.authorEmail}</span></p>
                        <h2 class="title">${this.title}</h2>
                        <h3 class="title-body">${this.body}</h3>
                        <button class="delete-btn">Delete</button>`
        return div;
    }

    deletePost(event){
        if(event.target.className === "delete-btn"){
            const currentId = event.target.closest('.post-wrapper').id
            fetch(`${postsApi}/${currentId}`,{
                method:'DELETE'
            }).then(response => {
                if(response.ok){
                   const div = document.getElementById(currentId)
                    div.remove()
                }
            }).catch(err => {console.log(err)})
        }
    }
}

const twitter = new Twitter(postsApi, usersApi);
twitter.getPosts()








