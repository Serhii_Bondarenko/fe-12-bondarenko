const body = document.querySelector('body')

const createDOMElement = (tag, parentElement) => {
    let element = document.createElement(tag);
    parentElement.append(element);
    return element;
}


createDOMElement('button', body).innerText ='Знайти по IP'
const button = document.querySelector('button')
button.addEventListener('click', () => showLocation())

const showLocation = async () => {
    try {
        const response = await fetch('https://api.ipify.org/?format=json');
        const {ip} = await response.json();
        getLocation(ip)
    }catch (err){
        console.log(err)
    }
}

 const getLocation = async (ip) => {
     try {
         const response = await fetch(`http://ip-api.com/json/${ip}?fields=continent,country,regionName,city`);
         const data = await response.json();
         showInfo (data)
     } catch (err) {
         console.log(err);
     }
 }


const showInfo = (obj, parentEl = body) => {
    const ul = createDOMElement('ul', parentEl);
    for (const [key, value] of Object.entries(obj)){
        const li = createDOMElement('li', ul);
        li.innerText = `${key}: ${value}`
    }
}

