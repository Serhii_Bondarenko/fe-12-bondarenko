const burger = document.querySelector('.burger');
const burgerMenu =document.querySelector('.burger-menu');

document.addEventListener('click', ()=>{
    if(event.target.classList.contains("burger-open") && event.target.classList.contains("burger") ){
        event.target.classList.remove("burger-open");
        burgerMenu.classList.remove('burger-menu-active')

    }
    else if(!event.target.classList.contains("burger-open") && event.target.classList.contains("burger")){
        event.target.classList.add("burger-open");
        burgerMenu.classList.add('burger-menu-active')
    }

    else if (event.target.className !=="menu-item" && event.target.className !=="menu-item-wrapper" && event.target.className !=="menu-item-list" ) {
        burger.classList.remove("burger-open")
        burgerMenu.classList.remove('burger-menu-active')
    }
})



const anchors = document.querySelectorAll('.menu-item');
anchors.forEach((item)=>item.addEventListener('click',(event)=>{
    event.preventDefault();
    const el=event.target.dataset.link
    const colorScroll = document.getElementById(el)
    colorScroll.scrollIntoView({
        behavior: "smooth",
        block: "center"
    });
    // colorScroll.style.cssText="background-color: white"


}))


// for (let anchor of anchors) {
//     anchor.addEventListener('click', function (e) {
//         e.preventDefault()
//
//         const blockID = anchor.getAttribute('href')
//
//         document.querySelector(blockID).scrollIntoView({
//             behavior: 'smooth',
//             block: 'start'
//         })
//     })
// }