const button = document.querySelector('.button');
const nav = document.querySelector('.nav');

button.addEventListener('click',() => {
    if(button.classList.contains('btn-show')){
        button.classList.replace('btn-show', 'btn-hide');
        nav.classList.add('nav-active');
    }else {
        button.classList.replace('btn-hide', 'btn-show');
        nav.classList.remove('nav-active');
    }
})