// let result = function (score){
//     return new Promise(function(resolve, reject){
//         console.log("Geathering results...");
//         if (score > 50) resolve("Gongrats, you have passed");
//         else reject("You are failed")
//     });
// };
//
// let grade = function(responce){
//     return new Promise(function(resolve, reject){
//         console.log("Calculating your grade...");
//         resolve("Your grade is A. " + responce);
//     });
// };

// result(49)
//     .then((responce) => {
//         console.log("Result received");
//         return grade(responce);
//     })
//     .then((finalgrade) => {
//         console.log((finalgrade));
//     })
//     .catch((err) => {
//         console.log(err);
//     })


//
// async function scope() {
//   try{
//       const responce = await result(80);
//       console.log(responce);
//       const finalgrade = await grade(responce)
//       console.log('++++',finalgrade);
//       // console.log(finalgrade);
//   }catch (err){
//       console.log(err)
//   }
// }
// scope()

// Задача1
// function fetchUser(userId) {
//     return new Promise((resolve, reject) => {
//       // Make a request to the API to fetch user data
//       api.get(/users/${userId})
//         .then((response) => {
//           resolve(response.data);
//         })
//         .catch((error) => {
//           reject(error);
//         });
//     });
//   }

// async function  fetchUser (userId){
//     try{
//         const response = await fetch(`/users/${userId}`);
//         return response.json();
//     }catch (err) {
//         return err
//     }
// }


//Задача 2
// function fetchPosts() {
//     return new Promise((resolve, reject) => {
//       // Make a request to the API to fetch posts data
//       api.get('/posts')
//         .then((response) => {
//           resolve(response.data);
//         })
//         .catch((error) => {
//           reject(error);
//         });
//     });
//   }

// async function  fetchPosts (){
//     try{
//         const response = await api.get('/posts');
//         return response.json();
//     }catch (err) {
//         return err
//     }
// }


//Задача3
// function createUser(user) {
//     return new Promise((resolve, reject) => {
//       // Make a request to the API to create a new user
//       api.post('/users', user)
//         .then((response) => {
//           resolve(response.data);
//         })
//         .catch((error) => {
//           reject(error);
//         });
//     });
//   }

// async function  createUser (user){
//     try{
//         const response = await fetch('/users',{method: 'POST'});
//         return response.json();
//     }catch (err) {
//         console.log(err);
//     }
// }

//Задача 4
// function updateUser(userId, user) {
//     return new Promise((resolve, reject) => {
//       // Make a request to the API to
//       api.put(/users/${userId}, user)
//       .then((response) => {
//         resolve(response.data);
//       })
//       .catch((error) => {
//         reject(error);
//       });
//   });
//   }

// async function  updateUser (userId, user){
//     try{
//         const response = await api.put(/users/${userId}, user) ;
//         return response.json();
//
//     }catch (err) {
//         console.log(err);
//     }
// }

//Задача 5
// function deleteUser(userId) {
//     return new Promise((resolve, reject) => {
//       // Make a request to the API to delete a user
//       api.delete(/users/${userId})
//         .then((response) => {
//           resolve(response.data);
//         })
//         .catch((error) => {
//           reject(error);
//         });
//     });
//   }

// async function  deleteUser (userId, user) {
//     try {
//         const response = await api.delete(/users/${userId});
//         return response.json();
//     } catch (err) {
//         console.log(err);
//     }
// }
//Task 6
// function fetchProducts() {
//     return new Promise((resolve, reject) => {
//       // Make a request to the API to fetch products data
//       api.get('/products')
//         .then((response) => {
//           resolve(response.data);
//         })
//         .catch((error) => {
//           reject(error);
//         });
//     });
//   }
//
// async function  fetchProducts () {
//     try {
//         const response = await api.get('/products');
//         return response.json();
//     } catch (err) {
//         console.log(err);
//     }
// }

//Task 7
// function createProduct(product) {
//     return new Promise((resolve, reject) => {
//       // Make a request to the API to create a new product
//       api.post('/products', product)
//         .then((response) => {
//           resolve(response.data);
//         })
//         .catch((error) => {
//           reject(error);
//         });
//     });
//   }

// async function  createProduct (product) {
//     try {
//         const response = await api.post('/products', product);
//         return response.json();
//     } catch (err) {
//         console.log(err);
//     }
// }

// Task 8
// function updateProduct(productId, product) {
//     return new Promise((resolve, reject) => {
//       // Make a request to the API to update a product
//       api.put(/products/${productId}, product)
//         .then((response) => {
//           resolve(response.data);
//         })
//         .catch((error) => {
//           reject(error);
//         });
//     });

// async function  updateProduct (productId, product) {
//     try {
//         const response = await api.put(/products/${productId}, product);
//         return response.json();
//     } catch (err) {
//         console.log(err);
//     }
// }


//Task 9
// function deleteProduct(productId) {
//     return new Promise((resolve, reject) => {
//       // Make a request to the API to delete a product
//       api.delete(/products/${productId})
//         .then((response) => {
//           resolve(response.data);
//         })
//         .catch((error) => {
//           reject(error);
//         });
//     });
//   }

// async function  deleteProduct (productId) {
//     try {
//         const response = await api.delete(/products/${productId});
//         return response.json();
//     } catch (err) {
//         console.log(err);
//     }
// }


//Task10
// function fetchOrders() {
//     return new Promise((resolve, reject) => {
//       // Make a request to the API to fetch orders data
//       api.get('/orders')
//         .then((response) => {
//           resolve(response.data);
//         })
//         .catch((error) => {
//           reject(error);
//         });
//     });
//   }
//
// async function  fetchOrders () {
//     try {
//         const response = await api.get('/orders');
//         return response.json();
//     } catch (err) {
//         console.log(err);
//     }
// }

// Задача 14
// напишіть форму реєстрації за адресою: "https://jsonplaceholder.typicode.com/users".
// У тілі запиту передавайте об'єкт із полями "email" "password".
// Використовуйте async/await.

// async function  registration (email, name ) {
//     try {
//         const response = await fetch('https://jsonplaceholder.typicode.com/users',
//             {
//             method: 'POST',
//             body:"form",
//             headers: {"Content-Type":"application/json, charset="UTF-8""}
//         }) ;
//         return response.json();
//     } catch (err) {
//         return err
//     }
// }

// getDevices = async () => {
//     const location = window.location.hostname;
//     const settings = {
//         method: 'POST',
//         headers: {
//             Accept: 'application/json',
//             'Content-Type': 'application/json',
//         }
//     };
//     try {
//         const fetchResponse = await fetch(`http://${location}:9000/api/sensors/`, settings);
//         const data = await fetchResponse.json();
//         return data;
//     } catch (e) {
//         return e;
//     }
//
// }