//
// try{
//     const fullName = prompt()
//     const arr = fullName.split(' ')
// }catch(err){
//     console.log(err);
// }
//
// console.log('++++')


////////////////////////////////////////////////////////////////////////////////
// Создайте функцию которая бы умела делать:
//     minus(10)(6); // 4
// minus(5)(6); // -1
// minus(10)(); // 10
// minus()(6); // -6
// minus()(); // 0
// Подсказка, функция minus должна возвращать другую функцию.

// function minus(num1 = 0){
//     return function  (num2 = 0){
//         return num1 - num2
//     }
// }
//
// const minusNew = minus(5)
// console.log(minusNew(5))

// Реализовать функцию, которая умножает и умеет запоминать возвращаемый результат между вызовами:
//     function multiplyMaker ...
// const multiply = multiplyMaker(2);
// multiply(2); // 4 (2 * 2)
// multiply(1); // 4 (4 * 1)
// multiply(3); // 12 (4 * 3)
// multiply(10); // 120 (12 * 10)

// function multiplyMaker(num1 = 0,num2 = 0){
//     let rezult = num1 * num2
//     return function  (num3 = 0){
//         return rezult * num3
//     }
// }
//
// const multiplyMakerNew = multiplyMaker(2,2)
// console.log(multiplyMakerNew(4))

//////////////////////////////////////////////////////////////////////////////
// Реализовать модуль, который работает со строкой и имеет методы:
//     a. установить строку
// i. если передано пустое значение, то установить пустую строку
// ii. если передано число, число привести к строке
// b. получить строку
// c. получить длину строки
// d. получить строку-перевертыш
// Пример:
//     модуль.установитьСтроку(‘abcde’);
// модуль.получитьСтроку(); // ‘abcde’
// модуль.получитьДлину(); // 5


///////////////////////////////////////////////////////////
// Напишите функцию createProduct, которая будет создавать объекты, описывающие товары. У товара должны быть такие свойства:
//     - name;
// - fullName;
// - article;
// - price.
//     При этом при попытке напрямую (через точку) изменить свойство price происходит его его проверка на прравильность:
//     цена должна быть целым положительным числом. Если эти требования нарушаются - присвоения не произойдет.
//     Создавать его аналог через _price нельзя.
//
//     Пример работы:
//     const notebook = createProduct("lenovo X120S", "lenovo X120S (432-44) W", 3332, 23244);
// console.log(notebook.price);// выведет  23244
// notebook.price = -4; // присвоение не произойдет
// console.log(notebook.price);// выведет  23244
// notebook.price = 22000;
// console.log(notebook.price);// выведет  22000

// function createProduct(name, fullName, article, price) {
//     class Product {
//         constructor(name, fullName, article, price) {
//             this._name = name;
//             this._fullName = fullName;
//             this._article = article;
//             this._price = price;
//         }
//         get price() {
//             return this._price;
//         }
//         set price(value) {
//             this._price =
//                 value > 0 && Number.isInteger(value)
//                     ? value
//                     : this._price;
//         }
//     }
//
//     return new Product(name, fullName, article, price);
// }
/////////////////////////////////////ver2/////////////////////////////
// function createProduct(name, fullName, article, price) {
//     let data = {
//         name: name,
//         fullName: fullName,
//         article: article,
//         price: price
//     };
//     return {
//         get name() {
//             return data.name;
//         },
//         set name(newName) {
//             data.name = newName;
//         },
//         get fullName() {
//             return data.fullName;
//         },
//         set fullName(newFullName) {
//             data.fullName = newFullName;
//         },
//         get article() {
//             return data.article;
//         },
//         set article(newArticle) {
//             data.article = newArticle;
//         },
//         get price() {
//             return data.price;
//         },
//         set price(newPrice) {
//             try {
//                 if (newPrice <= 0) {
//                     throw new Error("errr");
//                 }
//                 data.price = newPrice;
//             } catch (error) {
//                 console.error(error.message);
//             }
//         }
//     };
// }
//
// const notebook = createProduct("lenovo X120S", "lenovo X120S (432-44) W", 3332, 23244);
// console.log(notebook.price);
// notebook.price = -4;
// console.log(notebook.price);
// notebook.price = 10;
// console.log(notebook.price);

<!--Напишите функцию changeTextSize, у которой будут такие аргументы:-->
<!--1. Ссылка на DOM-элемент, размер текста которого нужно изменить без регистрации и sms.-->
<!--2. Величина в px, на которую нужно изменить текст,  возвращает функцию, меняющую размер текста на заданную величину.-->

<!--С помощью этой функции создайте две:-->
<!-- - одна увеличивает текст на 2px от изначального;-->
<!-- - вторая - уменьшает на 3px.-->

<!--После чего повесьте полученнные функции в качестве  обработчиков на кнопки с id="increase-text" и id="decrease-text".-->

// const text = document.querySelector('#text')
//
//
// function changeTextSize(domEl){
//     const carrentEl = parseInt(text.style.fontSize)
//      return function (fs){
//          return domEl.style.fontSize = `${carrentEl+fs}px`
//     }
//
// }
//
//
// const btnBid = document.querySelector('#increase-text');
// const btnmin = document.querySelector('#decrease-text');
//
// btnBid.addEventListener('click', (event)=>{
//     const changeTextSizeNew = changeTextSize(text)
//     console.log(changeTextSizeNew(2))
// })
//
// btnmin.addEventListener('click', ()=>{
//     const changeTextSizeNew = changeTextSize(text)
//     changeTextSizeNew(-3)
// })

// const changeTextSizeNew = changeTextSize(text)
// changeTextSizeNew(3)