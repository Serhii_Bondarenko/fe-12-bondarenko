// async function showAvatar() {
//
//     // Запитуємо JSON з даними користувача
//     let response = await fetch('/article/promise-chaining/user.json');
//     let user = await response.json();
//
//     // запитуємо інформацію про цього користувача з github
//     let githubResponse = await fetch(`https://api.github.com/users/${user.name}`);
//     let githubUser = await githubResponse.json();
//
//     // Відображаємо аватар користувача
//     let img = document.createElement('img');
//     img.src = githubUser.avatar_url;
//     img.className = "promise-avatar-example";
//     document.body.append(img);
//
//     // Чекаємо 3 секунди і потім приховуємо аватар
//     await new Promise((resolve, reject) => setTimeout(resolve, 3000));
//
//     img.remove();
//
//     return githubUser;
// }
//
// showAvatar();
