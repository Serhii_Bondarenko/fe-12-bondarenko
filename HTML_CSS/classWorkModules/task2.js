// Напишіть клас "Input", який створює об'єкт, що описує однорядковий рядок поле введення.
// Об'єкт матиме такі властивості:
// - тип поля введення;
// - класи;
// - id;
// і метод:
// - render, що повертає DOM-елемент, створений на основі властивостей об'єкта.
// Винесіть цей клас в окремий модуль, імпортуйте її в основний файл main.js,
// створіть однорядкове поле введення і виведіть його на екран.

// export default class Input{
//     constructor(typeValue, className, id) {
//         this.typeValue = typeValue;
//         this.className = className;
//         this.id = id
//     }
//      render (){
//         const body = document.querySelector('body');
//         const input = document.createElement('input');
//         input.type = this.typeValue;
//         input.className = this.className
//         input.id = this.id
//         body.append(input)
//
//     }
// }
//
// class NewInput extends Input{
//     constructor(text, password, email, checkbox) {
//         super(text);
//     }
//
// }



