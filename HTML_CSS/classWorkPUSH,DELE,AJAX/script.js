// const requestURL = 'https://ajax.test-danit.com/api/swapi/films'
//
// const headers = {'Content-Type': 'application/json', 'charset=UTF-8'};
//
// function sendRequest (method, url){
//     const headers = {'Content-Type': 'application/json'};
//     return fetch(url,{
//         method:method,
//         headers:headers
//     }).then(responsive => {
//         if(responsive.ok){
//             return responsive.json()
//         }else responsive.json().then(err =>{
//             const e = new Error ('Error');
//             e.data = err;
//             throw e
//         })
//     })
// }
//
//
// sendRequest('GET',requestURL)
//     .then(data => { console.log(data)})
//     .catch(err => {console.log(err)})

/////////////////////////////////////////////////////////////////////////////////////
// Створити to-do-list із кнопкою додати
// При додаванні тексту зберігати його в local storage
// та виводити на екран
// При відображенні якоїсь задачі потрібно добавити кнопки
// Edit та Delete

// let todoList = [];
//
// const renderTodoList = () => {
//   const todoListContainer = document.querySelector("#todo-list");
//   todoListContainer.innerHTML = "";
//
//   if (todoList.length === 0) {
//     todoListContainer.innerHTML = "<p>No items added yet.</p>";
//     return;
//   }
//
//   todoList.forEach((item, index) => {
//     const listItem = document.createElement("li");
//     listItem.textContent = item.text;
//
//     const editButton = document.createElement("button");
//     editButton.textContent = "Edit";
//     editButton.addEventListener("click", () => {
//       const editInput = document.createElement("input");
//       editInput.type = "text";
//       editInput.value = item.text;
//       editInput.addEventListener("keydown", (event) => {
//         if (event.key === "Enter") {
//           updateItem(index, { text: editInput.value });
//         } else if (event.key === "Escape") {
//           renderTodoList();
//         }
//       });
//       listItem.replaceChild(editInput, listItem.firstChild);
//       editInput.focus();
//     });
//
//     const deleteButton = document.createElement("button");
//     deleteButton.textContent = "Delete";
//     deleteButton.addEventListener("click", () => {
//       deleteItem(index);
//     });
//
//     listItem.appendChild(editButton);
//     listItem.appendChild(deleteButton);
//     todoListContainer.appendChild(listItem);
//   });
// };
//
// const addItem = (text) => {
//   return new Promise((resolve, reject) => {
//     const newItem = {
//       text: text,
//       compleate: false,
//     };
//
//     todoList.push(newItem);
//     renderTodoList();
//     localStorage.setItem("todoList", JSON.stringify(todoList));
//     resolve(newItem);
//   });
// };
//
// const updateItem = (index, updates) => {
//   return new Promise((resolve, reject) => {
//     Object.assign(todoList[index], updates);
//     renderTodoList();
//     localStorage.setItem("todoList", JSON.stringify(todoList));
//     resolve(todoList[index]);
//   });
// };
//
// const deleteItem = (index) => {
//   return new Promise((resolve, reject) => {
//     todoList.splice(index, 1);
//     renderTodoList();
//     localStorage.setItem("todoList", JSON.stringify(todoList));
//     resolve();
//   });
// };
//
// const addItemForm = document.querySelector("#add-item-form");
// const savedList = localStorage.getItem("todoList");
//
// if (savedList) {
//   todoList = JSON.parse(savedList);
//   renderTodoList();
// }
//
// addItemForm.addEventListener("submit", (event) => {
//   event.preventDefault();
//
//   const text = addItemForm.elements.text.value;
//
//   addItem(text)
//     .then(() => {
//       addItemForm.reset();
//     })
//     .catch((error) => {
//       console.error("Error adding item:", error);
//     });
// });










// const workersContainer = document.getElementById('workers');
// const paginationLinks = document.querySelectorAll('.pagination .page-link');
//
// let currentPage = 1;
// const workersPerPage = 20;

// // function to fetch and display workers
// function fetchWorkers(page) {
//     fetch(`https://jsonplaceholder.typicode.com/users?_page=${page}&_limit=${workersPerPage}`)
//         .then(response => response.json())
//         .then(data => {
//             // clear existing workers
//             workersContainer.innerHTML = '';
//
//             // create and append worker elements
//             data.forEach(worker => {
//                 const workerDiv = document.createElement('div');
//                 workerDiv.classList.add('worker');
//
//                 const nameElement = document.createElement('h3');
//                 nameElement.classList.add('worker-name');
//                 nameElement.textContent = worker.name;
//
//                 const jobElement = document.createElement('p');
//                 jobElement.classList.add('worker-job');
//                 jobElement.textContent = worker.company.name;
//
//                 const phoneElement = document.createElement('a');
//                 phoneElement.classList.add('worker-phone');
//                 phoneElement.textContent = worker.phone;
//                 phoneElement.href = `tel:${worker.phone}`;
//
//                 const emailElement = document.createElement('p');
//                 emailElement.classList.add('worker-email');
//                 emailElement.textContent = worker.email;
//
//                 workerDiv.appendChild(nameElement);
//                 workerDiv.appendChild(jobElement);
//                 workerDiv.appendChild(phoneElement);
//                 workerDiv.appendChild(emailElement);
//
//                 const deleteButton = document.createElement('button');
//                 deleteButton.textContent = 'Delete';
//                 deleteButton.addEventListener('click', () => {
//                     deleteWorker(worker.id);
//                 });
//
//                 workerDiv.appendChild(deleteButton);
//
//                 workersContainer.appendChild(workerDiv);
//             });
//         });
// }
//
// // initial fetch and display of workers
// fetchWorkers(currentPage);
//
// // add click event listeners to pagination links
// paginationLinks.forEach(link => {
//     link.addEventListener('click', () => {
//         currentPage = parseInt(link.textContent);
//         fetchWorkers(currentPage);
//     });
// });
//
// // function to add a new worker
// function addWorker(worker) {
//   fetch('https://jsonplaceholder.typicode.com/users', {
//     method: 'POST',
//     body: JSON.stringify(worker),
//     headers: {
//       'Content-type': 'application/json; charset=UTF-8',
//     },
//   })
//     .then(response => response.json())
//     .then(data => {
//       console.log(data);
//       alert('Worker added successfully!');
//       fetchWorkers(currentPage);
//     });
// }
//
// // function to delete a worker
// function deleteWorker(workerId) {
//     if (confirm('Are you sure you want to delete this worker?')) {
//         fetch(`https://jsonplaceholder.typicode.com/users/${workerId}`, {
//             method: 'DELETE',
//         })
//             .then((data) => { console.log(data)
//
//                 alert('Worker deleted successfully!');
//                 fetchWorkers(currentPage);
//             });
//     }
// }

// // function to update a worker
// function updateWorker(workerId, updatedWorker) {
//   fetch(`https://jsonplaceholder.typicode.com/users/${workerId}`, {
//     method: 'PUT',
//     body: JSON.stringify(updatedWorker),
//     headers: {
//       'Content-type': 'application/json; charset=UTF-8',
//     },
//   })
//     .then(response => response.json())
//     .then(data => {
//       console.log(data);
//       alert('Worker updated successfully!');
//       fetchWorkers(currentPage);
//     });
// }

