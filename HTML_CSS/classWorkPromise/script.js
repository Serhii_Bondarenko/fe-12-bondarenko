// const sleep = ms=>{
//     return new Promise (resolve => {
//         setTimeout(()=>resolve(), ms)
//     })
// }
//
// sleep(3000).then(() => console.log('hi'))
// sleep(6000).then(() => console.log('hi'))
// sleep(9000).then(() => console.log('hi'))

//
// let promise = new Promise(function(resolve, reject) {
//     setTimeout(() => reject(new Error("Whoops!")), 1000);
// });
//
// // reject запустить другу функцію, передану в .then
// promise.then(
//     result => alert(result), // не буде запущена
//     error => alert(error) // виведе "Error: Whoops!" через одну секунду
// );


// const embedImage = url => {
//     loadImage(url).then(function(result) {
//             const img = new Image();
//             let imageURL = window.URL.createObjectURL(result);
//             img.src = imageURL;
//             document.querySelector('body').appendChild(img);
//         },
//         function(err) {
//             console.log(err);
//         });
// }
// let allGood = true
// let promData = new Promise((resolve, reject) =>{
//     if(!allGood){
//         reject('err')
//     }else{
//         resolve({
//             id:1,
//             message:"Hi"
//         })
//     }
// })
//
// promData.then(fetchData => {
//     console.log('then', fetchData);
// }).catch(err=>{
//     console.error("catch", err)
// })

// let fetchSomeDate =function (data){
//     return new Promise((resolve, reject)=>{
//         setTimeout(()=>{
//             console.log('++++');
//             resolve({
//                 id:1,
//                 message:"good"
//             })
//         }, 1000)
//     })
// }
//
// let parseData = function (data){
//     return new Promise((resolve, reject)=>{
//         setTimeout(()=>{
//             let parseOut = `Analiz id ${data.id} is massage ${data.message}`;
//             resolve({parsed: parseOut});
//         }, 2000)
//     })
// }
//
// let echoData = function (data){
//     return new Promise((resolve, reject)=>{
//         setTimeout(()=> {
//             console.log(data)
//             console.log('--->',data.parsed)
//         }, 3000)
//     })
// }
//
// fetchSomeDate().then(parseData).then(echoData)

// const p1 = new Promise((resolve, reject)=>{
//     setTimeout(resolve, 3000, 'Hi')
// });
//
// const p2 = new Promise((resolve, reject)=>{
//     setTimeout(resolve,2000, 'Everybody')
// })
//
// const p3 = 1000;
//
// Promise.all([p1, p2, p3]).then((result)=>{
//     console.log(result)
// }).catch((err)=>{
//     console.error('err string', err)
// })




//1/////
// let age = +prompt("How old?")
//
// let promiseAge = new Promise((resolve, reject) =>{
//     if(age <= 18 ){
//         reject('go home')
//     }else{
//         resolve(" wellcome ")
//     }
// })
//
// promiseAge.then(res => console.log(res)).catch(res => console.log(res))



////2////
// В нас є список імен колег. Потрібно вивести в браузері з допомогою таблички(бажано)
// ім'я, фамілію, рейт $ per hour. Виводити через 2секунди весь контент.
// <ul id="data"></ul>
// Всі поля створюємо з допомогою js

const employees = [
    { name: "John", surname: "Doe", hourlyPay: 15 },
    { name: "Jane", surname: "Smith" },
    { name: "Bob", surname: "Johnson", hourlyPay: 25 },
    { name: "Sarah", surname: "Lee", hourlyPay: 18 },
    { name: "Mike", surname: "Brown", hourlyPay: 22 }
  ];



let employeesPromise = new Promise((resolve, reject)=>{
    setTimeout(()=>{
        employees.forEach(el=>{
            if(el.hourlyPay){
                console.log(`${el.name}, ${el.surname},${el.hourlyPay}`)
                resolve(`<p>${el.name}</p><p>${el.surname}</p><p>${el.hourlyPay}</p>`)
            }
            else{
                reject([el])
            }
        })
    },0)


})

employeesPromise.then(res=>console.log(res))