const body = document.querySelector('body')

const createDOMElement = (tag, parentElement) => {
    let element = document.createElement(tag);
    parentElement.append(element);
    return element;
}


createDOMElement('button', body).innerText ='Знайти по IP'
const button = document.querySelector('button')
button.addEventListener('click', (e)=> getIp())



async function getIp (){
    try {
        const response = await fetch('https://api.ipify.org/?format=json');
        let data = await response.json()
        geolocstion(data.ip);
    }catch (err){
        console.log(err)
    }
}



async function geolocstion(ip){
    try{
        const response = await fetch(`http://ip-api.com/json/${ip}?fields=continent,country,regionName,city`);
        let data = await response.json()
        render(data)
    }catch (err){
        console.log(err);
    }
}


const render = (obj) => {
    const ul = createDOMElement('ul', body);
    for (const [key, value] of Object.entries(obj)){
        const li = createDOMElement('li', ul);
        li.innerText = `${key}: ${value}`
    }


}

