import gulp from "gulp";
import gulpSass from "gulp-sass";
import dartSass from "sass";
import CleanCSS from "gulp-clean-css";
import rename from "gulp-rename";
import browsersync from "browser-sync";
import concat from "gulp-concat";

const sass = gulpSass(dartSass);

const buildCss = () => {
    return gulp
        .src("./src/scss/main.scss")
        .pipe(sass({ outputStyle: "expanded" }))
        .pipe(CleanCSS({ compatibility: "ie8" }))
        .pipe(
            rename({
                suffix: ".min",
                extname: ".css",
            })
        )
        .pipe(gulp.dest("./build/css"))
        .pipe(browsersync.stream());
};

const buildHtml = () => {
    return gulp
        .src("./src/index.html")
        .pipe(gulp.dest("./build"))
        .pipe(browsersync.stream());
};

const buildJs = () => {
    return gulp
        .src([
            "./src/js/header.js",
            "./src/js/footer.js",
        ])
        .pipe(concat("script.js"))
        .pipe(gulp.dest("./build/js"));
}

const watchJs = () => {
    gulp.watch("./src/js/*.js", buildJs);
}


//
// gulp.task("default", () => {
//   return gulp
//     .src("./src/scss/main.scss")
//     .pipe(sass({outputStyle: "expanded"}))
//     .pipe(gulp.dest("./build/css"))
//     .pipe(browsersync.stream());
// })

const watchCSS = () => {
    gulp.watch("./src/scss/**/*.scss", buildCss)
}

const watchHTML = () => {
    gulp.watch("./src/*.html", buildHtml)
}

gulp.task("default", gulp.parallel(watchCSS, watchHTML,watchJs, buildJs));
// gulp.task("default", gulp.series(watchCSS, watchHTML, buildJs));
