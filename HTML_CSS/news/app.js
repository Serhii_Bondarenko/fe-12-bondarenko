// // Custom Http Module
//
// // 9c9c5f0d69ca41c2924a2d83a81592de
//
// function customHttp() {
//   return {
//     get(url, cb) {
//       try {
//         const xhr = new XMLHttpRequest();
//         xhr.open('GET', url);
//         xhr.addEventListener('load', () => {
//           if (Math.floor(xhr.status / 100) !== 2) {
//             cb(`Error. Status code: ${xhr.status}`, xhr);
//             return;
//           }
//           const response = JSON.parse(xhr.responseText);
//           cb(null, response);
//         });
//
//         xhr.addEventListener('error', () => {
//           cb(`Error. Status code: ${xhr.status}`, xhr);
//         });
//
//         xhr.send();
//       } catch (error) {
//         cb(error);
//       }
//     },
//     post(url, body, headers, cb) {
//       try {
//         const xhr = new XMLHttpRequest();
//         xhr.open('POST', url);
//         xhr.addEventListener('load', () => {
//           if (Math.floor(xhr.status / 100) !== 2) {
//             cb(`Error. Status code: ${xhr.status}`, xhr);
//             return;
//           }
//           const response = JSON.parse(xhr.responseText);
//           cb(null, response);
//         });
//
//         xhr.addEventListener('error', () => {
//           cb(`Error. Status code: ${xhr.status}`, xhr);
//         });
//
//         if (headers) {
//           Object.entries(headers).forEach(([key, value]) => {
//             xhr.setRequestHeader(key, value);
//           });
//         }
//
//         xhr.send(JSON.stringify(body));
//       } catch (error) {
//         cb(error);
//       }
//     },
//   };
// }
// // Init http module
// const http = customHttp();
//
// const newsService = (function (){
//   const API_KEY = '9c9c5f0d69ca41c2924a2d83a81592de';
//   const API_URL = 'https://newsapi.org/v2';
//
//   return{
//     topHeadlines(country = 'ua', callback){
//       http.get(`${API_URL}/top-headlines?country=${country}&apiKey=${API_KEY}`, callback);
//     },
//     everything(query, callback){
//       http.get(`${API_URL}/everything?q=${query}&apiKey=${API_KEY}`, callback);
//     }
//
//   };
// })();
//
//
//
// //  init selects
// document.addEventListener('DOMContentLoaded', function() {
//   M.AutoInit();
//   loadNews();
// });
//
//
// //becend
// function loadNews(){
//   newsService.topHeadlines('ua', onGetResponse)
// }
//
// function onGetResponse(err, response){
//   if(err) {
//     console.error('err');
//     return
//   }
//   const isLoad = isStatusOk(response.status)
//
//   if(!isLoad){
//     console.error('Something went wrong!!');
//     return
//   }
//
//   renderNews(response.articles)
//   // https://newsapi.org/v2/top-headlines/sources?category=businessapiKey=API_KEY
// }
//
//
//
//
// function renderNews (news) {
//   let fragment= ''
//   if(!news.length){
//     fragment+='No news'
//   }
//   const newsContainer = document.querySelector('.news-container .row')
//   news.forEach((article)=>{
//     const newsItem = createNewsTemplate(article)
//     fragment += newsItem
//   })
//
//
//   newsContainer.insertAdjacentHTML('afterbegin', fragment)
// }
//
// function createNewsTemplate({ urlToImage, url, title = '', description = '' }){
//   const  DEFAULT_IMG = "https://www.northampton.ac.uk/wp-content/uploads/2018/11/default-svp_news.jpg";
//   return `
//   <article class="col s12">
//     <div class="card">
//       <div class="card-image">
//         <img src='${urlToImage || DEFAULT_IMG}' alt='news image'/>
//         <h2>${title}</h2>
//       </div>
//       <div class="card-content">
//         <p>${description}</p>
//       </div>
//       <div class="card-action">
//         <a href="${url}" target="_blank">Read more</a>
//       </div>
//     </div>
//   </article>`;
//
//
// }
//
// // function isStatusOk(status){
// //   return status === 'ok'
// // }
//
// const inputField = document.querySelector('#autocomplete-input')
// const btnSeach = document.querySelector('.btn')
// btnSeach.addEventListener('click', (event)=>{
//   event.preventDefault()
//   newsService.everything(inputField.value, onGetResponse)
// })
//
// const dropdown = document.querySelector('#country');
// dropdown.addEventListener('change',(evt)=>{
//   evt.preventDefault()
//   newsService.topHeadlines(dropdown.value, onGetResponse)
//
// })


