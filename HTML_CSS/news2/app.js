// Custom Http Module

// 9c9c5f0d69ca41c2924a2d83a81592de



function apiNews(country='ua', query = '', typeNews) {

  const API_KEY = '9c9c5f0d69ca41c2924a2d83a81592de';
  const API_URL = 'https://newsapi.org/v2';
  const topHeadlines = `${API_URL}/top-headlines?country=${country}&apiKey=${API_KEY}`
  const everything = `${API_URL}/everything?q=${query}&apiKey=${API_KEY}`
  if (typeNews === 'everythingNews') {
    getNews(everything)
  } else {
    getNews(topHeadlines)
  }

}


async function getNews(url ='https://newsapi.org/v2/top-headlines?country=ua&apiKey=9c9c5f0d69ca41c2924a2d83a81592de') {
  try{
    const responce = await fetch(url);
    const data = await responce.json()
    renderNews(data.articles);

  }catch (err){
    console.log(err)
  }
}




document.addEventListener('DOMContentLoaded', function() {
  M.AutoInit();
  getNews()

});



function renderNews(news){
  let fragment= ''
  if(!news.length){
    fragment+='No news'
  }
  const newsContainer = document.querySelector('.news-container .row')
  news.forEach((article)=>{

    const newsItem = createNewsTemplate(article)
    fragment += newsItem
  })


  newsContainer.insertAdjacentHTML('afterbegin', fragment)
}

function createNewsTemplate({ urlToImage, url, title = '', description = '' }){
  const  DEFAULT_IMG = "https://www.northampton.ac.uk/wp-content/uploads/2018/11/default-svp_news.jpg";
  return `
  <article class="col s12">
    <div class="card">
      <div class="card-image">
        <img src='${urlToImage || DEFAULT_IMG}' alt='news image'/>
        <h2>${title}</h2>
      </div>
      <div class="card-content">
        <p>${description}</p>
      </div>
      <div class="card-action">
        <a href="${url}" target="_blank">Read more</a>
      </div>
    </div>
  </article>`;


}



const inputField = document.querySelector('#autocomplete-input')
const btnSeach = document.querySelector('.btn')
btnSeach.addEventListener('click', (event)=>{
  event.preventDefault()

  apiNews( '', inputField.value,'everythingNews')



})

const dropdown = document.querySelector('#country');
dropdown.addEventListener('change',(evt)=>{
  evt.preventDefault()
  apiNews( dropdown.value, '','topHeadlines')


})


