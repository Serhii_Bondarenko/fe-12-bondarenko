const url = 'https://ajax.test-danit.com/api/swapi/films'


const getDataByUrl = (url,ul) => {
    const headers = {'Content-Type': 'application/json'};
    return fetch(url,{
        method:'GET',
        headers
    }).then(response => {
        if (response.ok) {
            return response.json()
        }
        throw  new Error(`Error ${response.statusText}`);
    }).then(data => {
        const liName = document.createElement('li')
        liName.innerHTML = data.name
        ul.append(liName)
    });
}
///////////////////////////////////////////////////////////////////////////////////////
function getCharacter (url) {
    const headers = {'Content-Type': 'application/json'};
    fetch(url, {
        method: 'GET',
        headers: headers
    }).then(response => {
        if (response.ok) {
            return response.json()
        }
        throw  new Error(`Error ${response.statusText}`);
    }).then(data=>{
            console.log(data);
            const body = document.querySelector('body')
            data.forEach(characterUrl =>{
                const ul = document.createElement('ul')
                const liEpisode = document.createElement('li')
                liEpisode.innerHTML = characterUrl.episodeId
                ul.append(liEpisode)
                const liOpeningCrawl = document.createElement('li')
                liOpeningCrawl.innerHTML = characterUrl.openingCrawl
                ul.append(liOpeningCrawl)

                characterUrl.characters.forEach(item => getDataByUrl(item,ul))
                body.append(ul)
            })
    }
    ).catch(err => console.error(err))
}



getCharacter(url);
//
//
//
// function getCharacters (url){
//     const headers = {'Content-Type': 'application/json'};
//     fetch(url,{
//         method: 'GET',
//         headers:headers
//     }).then(response => {
//         if (response.ok) {
//             return response.json()
//         }
//         throw  new Error(`Error ${response.statusText}`);
//     }).then(data => console.log(data));
// }


///////////////////////////////////////////////////////////////////////////////////


// function render (arr){
//     console.log(arr)
    // const arrCharacters = arr.filter((element) => element.characters)
    // console.log(arrCharacters);

    // const body = document.querySelector('body');
    // const ul = document.createElement('ul');
    // body.append(ul)
    // const episode = arr.map((item)=>{
    //     return  `<li>episode ${item.episodeId}</li>
    //             <li>${item.openingCrawl}</li>`
    //
    // })
//     // ul.innerHTML = episode.join('')
// }
//     // console.log();

    // arr.forEach((item) =>{
    //     const li = document.createElement('li')
    //     const episode = item.episodeId.innerHTML;
    //     li.append(episode)
    //     // li.innerHTML = item.openingCrawl + item.episodeId
    //     // li.innerHTML = item.episodeId;
    //     // ul.append(li)
    //     // const liId = document.createElement('li')
    //     // li.innerHTML = item.episodeId;
    //     // ul.append(liId)
    // })


//*************************************************//////////////////WORK!!!!!!//////////

// const body = document.querySelector('body')
//
// async function getCharactersData(){
//     try{
//         const response = await fetch(url);
//         let data = await response.json();
//         console.log(data);
//         data.forEach(elFilms =>{
//             const ul = document.createElement('ul');
//             body.append(ul)
//             const liEpisodeId = document.createElement('li');
//             liEpisodeId.innerHTML = `episode ${elFilms.episodeId}`;
//             ul.append(liEpisodeId);
//             const liOpeningCrawl = document.createElement('li');
//             liOpeningCrawl.innerHTML = elFilms.openingCrawl;
//             ul.append(liOpeningCrawl);
//             elFilms.characters.forEach(character => getByUrl(character, ul));
//         })
//     }
//     catch (err) {
//         console.log(err);
//     }
// }
//
// getCharactersData()
//
//
// async function getByUrl(url,ul){
//     try {
//         const character = await fetch(url);
//         let data = await character.json();
//         const liName = document.createElement('li')
//         liName.innerHTML = data.name;
//         ul.append(liName)
//     }catch (err){
//         console.log(err);
//     }
// }

