const usersApi = 'https://ajax.test-danit.com/api/json/users';
const postsApi = 'https://ajax.test-danit.com/api/json/posts';
const body = document.querySelector('body')

function getUsers (url) {
    const headers = {'Content-Type': 'application/json'}
    return fetch(url, {
        headers: headers
    }).then(responsive => {
        if (responsive.ok) {
            return responsive.json()
        }
        throw  new Error(`Error ${responsive.statusText}`);
    }).then(posts => {
        posts.forEach(post =>{
            // console.log(post.userId);
            const headers = {'Content-Type': 'application/json'}
            return fetch(usersApi,{
                headers: headers
            }).then(responsive =>{
                if (responsive.ok){
                    return responsive.json()
                }
                throw  new Error(`Error ${responsive.statusText}`);
            }).then(users => {
                console.log(users);
                const currentUser = users.find(user =>user.id === post.userId)
                const card = new Card(post.id, post.title, post.body,currentUser.name, currentUser.email).render()
                body.append(card)

            })

        })

    })


}

class Card{
    constructor(id, title, content, authorName, authorEmail) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.authorName = authorName;
        this.authorEmail = authorEmail;
    }
    render(){
        const wpapperPost = document.createElement('div')
        wpapperPost.addEventListener('click', ev => this.delete(ev))
        wpapperPost.id = `${this.id}`
        wpapperPost.innerHTML = `<h1>Author Post ${this.authorName}</h1>
                                <h3>Author Email${this.authorEmail}</h3>
                                <h2>Author title${this.title}</h2>
                                <p>Author body${this.content}</p>
                                <button class="delete-btn">Delete</button>`
        return wpapperPost
    }
    delete(ev){
        if(ev.target.className ==='delete-btn'){
            const postId = ev.target.closest("div").id
            fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
                method: 'DELETE',
            })
                .then(responsive =>{
                    if(responsive.ok){
                        console.log(responsive.status)
                        const elDel = document.getElementById(`${postId}`)
                        elDel.remove()
                    }
                })
        }
    }
}

getUsers(postsApi)

// getUsers(usersApi)





