
class Table{
    constructor() {
        this.countId = 0;
        this.cardsBlock = document.querySelector('.cards-wrapper')
        this.input =  document.querySelector('#input-notates');
        this.submit =  document.querySelector('#submit');

    }
    createNote(){
        this.submit.addEventListener('click', (event)=>{
            event.preventDefault()
            if(this.input.value){
                const notates = new Note(this.countId, this.input.value).render()
                this.cardsBlock.append(notates)
                this.input.value = ""
                this.countId++
            }
        })
    }
}



class Note{
    constructor(id , value,) {
        this.id = id;
        this.value = value;
    }
    render(){
        const div = document.createElement('div');
        div.id = this.id;
        div.classList.add('card')
        const p = document.createElement('p');
        p.innerText = this.value
        div.append(p)
        const btnEdit = document.createElement('button');
        btnEdit.innerText = 'Edit'
        div.append(btnEdit)
        const btnDelete = document.createElement('button');
        btnDelete.innerText = 'Delete'
        div.append(btnDelete)
        btnEdit.addEventListener('click', ev => {
            this.edit(ev, p, div, btnEdit, btnDelete)
        })
        btnDelete.addEventListener('click', ev => this.done(div))

        return div
    }
    edit(ev, p, div, btnEdit, btnDelete){
            p.remove()
            btnEdit.remove()
            btnDelete.remove()
            const editInput = document.createElement('input');
            editInput.setAttribute("type", "text");
            editInput.setAttribute("placeholder", `${this.value}`);
            div.append(editInput)
            const btnDoneEdit = document.createElement('button');
            btnDoneEdit.innerText = 'Done Edit'
            div.append(btnDoneEdit)
            btnDoneEdit.addEventListener('click', ()=>{
                editInput.remove()
                btnDoneEdit.remove()
                div.append(p)
                p.innerText = editInput.value
                div.append(btnEdit)
                div.append(btnDelete)
                console.log(editInput.value);
            })
    }
    done(div){
        div.remove()
    }

}

const board = new Table()
board.createNote()

